exports.handler = (event, context, callback) => {
    var Twitter = require('twitter');

    var client = new Twitter({
        consumer_key: process.env.TWITTER_CONSUMER_KEY,
        consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
        access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
        access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
    });

    var twitter_params = {
        screen_name: process.env.TWITTER_USERNAME,
        count: 1
    };

    client.get('statuses/user_timeline', twitter_params, function(err, tweets) {
        if (!err) {
            var AWS = require('aws-sdk');

            AWS.config.update({
                accessKeyId: process.env.S3_ACCESS_KEY_ID,
                secretAccessKey: process.env.S3_SECRET_ACCESS_KEY
            });

            var s3 = new AWS.S3();

            var s3_read_params = {
                Bucket: process.env.S3_BUCKET_NAME,
                Key: twitter_params.screen_name + ".json"
            };


            var checkExistingTweet = s3.getObject(s3_read_params).promise();

            checkExistingTweet.then(function(resp) {
                // Comparing the id values of the latest tweet and the saved one.
                // If the ids are identical, the tweet does not need to be saved again.
                if (JSON.parse(resp.Body.toString()).id == tweets[0].id) {
                    callback(null, {
                        statusCode: 200,
                        body: JSON.stringify("Latest tweet is already saved.")
                    });
                } else {
                    // Tweets are not identical, so latest will be saved.
                    var tweet_json = {
                        id: tweets[0].id,
                        text: tweets[0].text
                    }

                    var s3_write_params = {
                        Body: JSON.stringify(tweet_json),
                        Bucket: s3_read_params.Bucket,
                        Key: s3_read_params.Key
                    };

                    s3.putObject(s3_write_params, function(err, resp) {
                        if (!err) {
                            callback(null, {
                                statusCode: 200,
                                body: JSON.stringify("Latest tweet from " + twitter_params.screen_name + " saved successfully.")
                            });
                        } else {
                            callback(null, {
                                statusCode: 500,
                                body: JSON.stringify("An error occurred while attempting to save the latest tweet in S3.")
                            });
                        }
                    });
                }
            }).catch(function(err) {
                // File not found, saving first tweet.
                var tweet_json = {
                    id: tweets[0].id,
                    text: tweets[0].text
                }

                var s3_write_params = {
                    Body: JSON.stringify(tweet_json),
                    Bucket: s3_read_params.Bucket,
                    Key: s3_read_params.Key
                };

                s3.putObject(s3_write_params, function(err, resp) {
                    if (!err) {
                        callback(null, {
                            statusCode: 200,
                            body: JSON.stringify("Latest tweet from " + twitter_params.screen_name + " saved successfully.")
                        });
                    } else {
                        console.log("An error occurred while attempting to save the latest tweet in S3.");

                        callback(null, {
                            statusCode: 500,
                            body: JSON.stringify("An error occurred while attempting to save the latest tweet in S3.")
                        });
                    }
                });
            })
        }
    });
};
