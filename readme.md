## Startup

 - Sign up for a Twitter developer account. Then create a Twitter app and store the consumer and access keys for future reference.
 - In the AWS console look for the S3 product page. Create a named bucket ensuring that the name does not conflict with others. The default settings are sufficient for running the lambda function.
 - After the bucket has been created, search for the IAM product page. Click the users link on the sidebar, then click **add user**.
 - Choose a username to use for the function. Under AWS access type choose **programmatic access**.
 - In the next page, click create group. Specify a name for the group. Then in the search box next to filter policies, search for S3. Select the **AmazonS3FullAccess** policy. Then create the group.
 - Once the account has been created, click on the name of the account just created. Then click create security credentials and then create access key. Save both values closely for the next couple of steps.
 - After the AWS access keys have been created, navigate to the AWS Lambda product page.
 - Click on **Function**. Then name the function **tweetbot**. Change the role drop down menu option to **create new role from templates**.
 - For the role name field, utilize **bots** as the value. Then create the function.
 
## Lambda

 - Before utilizing the lambda code please install the node modules by executing the following command `npm install`.
 - To utilize the lambda code zip up all of the contents of the tweetbot folder excluding the folder itself. Then upload the file to the lambda page then save.
 - Underneath the environmental variables section specific the following keys to reference your account specific values. PS the TWITTER_USERNAME value just refers to the account the tweetbot will download tweets for.

    ~~~~
    S3_ACCESS_KEY_ID
    S3_BUCKET_NAME
    S3_SECRET_ACCESS_KEY
    TWITTER_ACCESS_TOKEN_KEY
    TWITTER_ACCESS_TOKEN_SECRET
    TWITTER_CONSUMER_KEY
    TWITTER_CONSUMER_SECRET
    TWITTER_USERNAME
    ~~~~
    
 - After the values have been specified, add a Cloudwatch event trigger to the function. Create a new rule and set the rule's type to schedule expression. Set the rate of execution to **rate(1 day)** then click save.
 
## Improvements

 - Having more free time I'd like to refactor the code to optimize it a bit. Some lines of code that are duplicated can be eliminated. The code needs to be unit testable as well so refactoring can allow to break units of work into small pieces.
 - Add unit tests, goes in hand with prior comment.
 - Implement an API to change the twitter username environment variable.
 - Utilize more features of the Twitter API client like retweeting the latest tweet it received from tjhe specified user.
 - Perform some analytics on how much time it took to execute the lambda function. It could help visualize what optimization is needed to speed up the function (eg time the function).
